# Generated by Django 2.1.3 on 2018-12-17 11:00

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_ipcop_models", "0006_convert_comments_to_larger_textfield"),
    ]

    operations = [
        migrations.AddField(
            model_name="ipcopabuseip4",
            name="ticket_id",
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="ipcopabuseip4",
            name="ticket_last_sent",
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name="ipcopabuseip6",
            name="ticket_id",
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="ipcopabuseip6",
            name="ticket_last_sent",
            field=models.DateTimeField(null=True),
        ),
    ]
