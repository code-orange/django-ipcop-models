# Generated by Django 2.1.3 on 2018-11-30 12:05

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_ipcop_models", "0002_add_sync_partner"),
    ]

    operations = [
        migrations.AddField(
            model_name="ipcopabuseip4",
            name="sync_partners_out",
            field=models.ManyToManyField(to="django_ipcop_models.IpcopSyncPartner"),
        ),
        migrations.AddField(
            model_name="ipcopabuseip6",
            name="sync_partners_out",
            field=models.ManyToManyField(to="django_ipcop_models.IpcopSyncPartner"),
        ),
    ]
