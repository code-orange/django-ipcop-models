from datetime import datetime

from django.db import models


class IpcopAbuseCategory(models.Model):
    category = models.CharField(max_length=250)

    class Meta:
        db_table = "ipcop_abuse_category"


class IpcopSyncPartner(models.Model):
    category = models.CharField(max_length=250)

    class Meta:
        db_table = "ipcop_sync_partner"


class IpcopAbuseIp4(models.Model):
    id = models.BigAutoField(primary_key=True)
    ip = models.ForeignKey("IpcopKnownIp4", models.DO_NOTHING, db_column="ip")
    categories = models.ManyToManyField("IpcopAbuseCategory")
    sync_partners_out = models.ManyToManyField("IpcopSyncPartner")
    comment = models.TextField(blank=True, null=True)
    report_received = models.DateTimeField(null=False, default=datetime.now)

    class Meta:
        db_table = "ipcop_abuse_ip4"


class IpcopAbuseIp6(models.Model):
    id = models.BigAutoField(primary_key=True)
    ip = models.ForeignKey("IpcopKnownIp6", models.DO_NOTHING, db_column="ip")
    categories = models.ManyToManyField("IpcopAbuseCategory")
    sync_partners_out = models.ManyToManyField("IpcopSyncPartner")
    comment = models.TextField(blank=True, null=True)
    report_received = models.DateTimeField(null=False, default=datetime.now)

    class Meta:
        db_table = "ipcop_abuse_ip6"


class IpcopKnownIp4(models.Model):
    id = models.BigAutoField(primary_key=True)
    ipaddr = models.GenericIPAddressField(unique=True, protocol="IPv4")
    first_seen = models.DateTimeField(null=False, default=datetime.now)
    ticket_id = models.IntegerField(blank=True, null=True)
    ticket_last_sent = models.DateTimeField(null=True)

    class Meta:
        db_table = "ipcop_known_ip4"


class IpcopKnownIp6(models.Model):
    id = models.BigAutoField(primary_key=True)
    ipaddr = models.GenericIPAddressField(unique=True, protocol="IPv6")
    first_seen = models.DateTimeField(null=False, default=datetime.now)
    ticket_id = models.IntegerField(blank=True, null=True)
    ticket_last_sent = models.DateTimeField(null=True)

    class Meta:
        db_table = "ipcop_known_ip6"


class IpcopFilterIp4(models.Model):
    id = models.AutoField(primary_key=True)
    cidr = models.TextField(unique=True, max_length=18, null=False, blank=False)
    add_date = models.DateTimeField(null=False, default=datetime.now)

    class Meta:
        db_table = "ipcop_filter_ip4"


class IpcopFilterIp6(models.Model):
    id = models.AutoField(primary_key=True)
    cidr = models.TextField(unique=True, max_length=255, null=False, blank=False)
    add_date = models.DateTimeField(null=False, default=datetime.now)

    class Meta:
        db_table = "ipcop_filter_ip6"
